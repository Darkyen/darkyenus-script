import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 10:01 AM
 */
public class alias implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            String alias = arguments[0];
            if(arguments.length >= 2){
                String command = arguments[1];
                reference.addAlias(alias,command);
            }else{
                reference.removeAlias(alias);
            }
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "alias <alias> [command]\n" +
                "Creates or removes aliased command. Add command (in string form, not as a command!) to create, leave it blank to remove.";
    }
}
