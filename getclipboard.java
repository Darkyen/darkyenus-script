import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:47 AM
 */
public class getclipboard implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            String result = (String)Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            if(result == null){
                return "null";
            }else{
                return result;
            }
        } catch (Exception ex) {
            if (arguments.length < 1 || !arguments[0].equalsIgnoreCase("mute")) {
                System.out.println("Could not retrieve clipboard: "+ex);
            }
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "getclipboard [mute]\n" +
                "Retrieves system clipboard, string flavour.\n" +
                "Add 'mute' parameter to not print potential errors.";
    }
}
