import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 8:09 AM
 */
public class replace implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            String string = arguments[0];
            String from = arguments[1];
            String to = arguments[2];

            return string.replace(from,to);
        } catch (Exception ex) {
            System.out.println("Invalid usage, error occurred. "+ex);
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "replace <string> <from> <to>\n" +
                "All occurrences of <from> in <string> will be replaced with <to>.";
    }
}
