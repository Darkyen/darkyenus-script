
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class divide implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int result = Integer.parseInt(arguments[0]);
            for (int i = 1; i < arguments.length; i++) {
                String value = arguments[i];
                result /= Integer.parseInt(value);
            }
            return Integer.toString(result);
        } catch (Exception numberFormatException) {
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "divide <arguments>\nDivides first argument by second, then by third etc. Returns null if non number found or zero is one of divisors.";
    }
    
}
