import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import javax.swing.*;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 8:46 AM
 */
public class guimessage implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            String content = arguments[0];
            String title = arguments.length >= 2?arguments[1]:"";
            int type = JOptionPane.PLAIN_MESSAGE;
            if(arguments.length >= 3){
                if(arguments[2].equalsIgnoreCase("plain")){
                    type = JOptionPane.PLAIN_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("error")){
                    type = JOptionPane.ERROR_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("warning")){
                    type = JOptionPane.WARNING_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("info")){
                    type = JOptionPane.INFORMATION_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("question")){
                    type = JOptionPane.QUESTION_MESSAGE;
                }
            }

            JOptionPane.showMessageDialog(null, content,title,type);
        } catch (Exception ex) {
            System.out.println("Could not show message: "+ex);
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "guimessage <content> [title] [type]\n" +
                "Shows a window with given message and properties.\n" +
                "Type alters layout/icon and can be one of these:\n" +
                "plain (default), error, warning, info, question";
    }
}
