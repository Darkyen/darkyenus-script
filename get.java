
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class get implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length != 1){
            System.out.println("Invalid amount parameters.");
        }else{
            String result = reference.getVariable(arguments[0]);
            if(result != null){
                return result;
            }
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "get <variable>\nGets value of variable. If variable doesn't exist, returns string null.";
    }
    
}
