import darkyenus.dipt.binding.BindManager;
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/14/13
 * Time: 10:00 PM
 */
public class unbind implements Command{
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            if(arguments[0].equalsIgnoreCase("all")){
                BindManager.manager.unbindAll();
                return "true";
            }else{
                int toUnbind = Integer.parseInt(arguments[0]);
                return BindManager.manager.unbind(toUnbind)?"true":"false";
            }
        } catch (Exception ex) {
            System.out.println(getHelp());
            return "false";
        }
    }

    @Override
    public String getHelp() {
        return "unbind <all | id>\n" +
                "Unbinds action of given id (returned by bind commands) or all actions.";
    }
}
