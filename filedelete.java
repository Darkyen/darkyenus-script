import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.io.File;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:58 AM
 */
public class filedelete implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            File pathAnalyzer = new File(arguments[0]);
            File newFile;
            if(pathAnalyzer.isAbsolute()){
                newFile = pathAnalyzer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }

            return Boolean.toString(newFile.delete());
        } catch (Exception ex) {
            if (arguments.length < 1 || !arguments[0].equalsIgnoreCase("mute")) {
                System.out.println("Could not read file: "+ex);
            }
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "filedelete <path to file> [mute]\n" +
                "Deletes given file.\n" +
                "Specify [mute] flag to not print potential errors.\n" +
                "Returns true if removal was successful.";
    }
}
