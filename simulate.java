import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.awt.*;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 7:07 AM
 */
public class simulate implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            if(arguments[0].equalsIgnoreCase("key")){
                boolean press = false;
                if(arguments[1].equalsIgnoreCase("+") || arguments[1].equalsIgnoreCase("press")){
                    press = true;
                }
                int keyCode = Integer.parseInt(arguments[2]);
                boolean waits = true;
                if(arguments.length >= 4 && arguments[3].equalsIgnoreCase("instant")){
                    waits = false;
                }

                if(press){
                    getRobot().keyPress(keyCode);
                    if(waits){
                        getRobot().waitForIdle();
                    }
                }else{
                    getRobot().keyRelease(keyCode);
                    if(waits){
                        getRobot().waitForIdle();
                    }
                }
            }else if(arguments[0].equalsIgnoreCase("button")){
                boolean press = false;
                if(arguments[1].equalsIgnoreCase("+") || arguments[1].equalsIgnoreCase("press")){
                    press = true;
                }
                int buttonCode = Integer.parseInt(arguments[2]);
                boolean waits = true;
                if(arguments.length >= 4 && arguments[3].equalsIgnoreCase("instant")){
                    waits = false;
                }

                if(press){
                    getRobot().mousePress(buttonCode);
                    if(waits){
                        getRobot().waitForIdle();
                    }
                }else{
                    getRobot().mouseRelease(buttonCode);
                    if(waits){
                        getRobot().waitForIdle();
                    }
                }
            }else if(arguments[0].equalsIgnoreCase("wheel")){
                int amount = Integer.parseInt(arguments[1]);
                boolean waits = true;
                if(arguments.length >= 3 && arguments[2].equalsIgnoreCase("instant")){
                    waits = false;
                }
                getRobot().mouseWheel(amount);
                if(waits){
                    getRobot().waitForIdle();
                }
            }else if(arguments[0].equalsIgnoreCase("mouse")){
                int x = Integer.parseInt(arguments[1]);
                int y = Integer.parseInt(arguments[2]);

                boolean waits = true;
                if(arguments.length >= 4 && arguments[3].equalsIgnoreCase("instant")){
                    waits = false;
                }
                getRobot().mouseMove(x,y);
                if(waits){
                    getRobot().waitForIdle();
                }
            }
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "simulate <key | button | wheel | mouse>\n" +
                "     key < + | - > <key code> [instant]\n" +
                "     button < + | - > <button code> [instant]\n" +
                "     wheel <amount> [instant]\n" +
                "     mouse <x> <y> [instant]\n" +
                "Simulates key and button press and release. Can also simulate mouse wheel movement and mouse movement.";
    }


    private static Robot robot;

    public static Robot getRobot() throws AWTException {
        if(robot == null){
            robot = new Robot(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice());
        }
        return robot;
    }
}
