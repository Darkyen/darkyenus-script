
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;
import java.io.File;


/**
 *
 * @author Darkyen
 */
public class cd implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length != 1){
            System.out.println("Invalid amount parameters.");
        }else{
            File pathAnalizer = new File(arguments[0]);
            File newFile;
            if(pathAnalizer.isAbsolute()){
                newFile = pathAnalizer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }
            if(newFile.isDirectory()){
                reference.setWorkingDirectory(newFile);
            }else{
                System.out.println("Invalid directory path.");
            }
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "cd <path>\nChanges working directory to path, may be relative or absolute.";
    }
    
}
