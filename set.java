
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class set implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length != 2){
            System.out.println(getHelp());
        }else{
            reference.setVariable(arguments[0], arguments[1]);
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "set <variable> <value>\nSets temporary variable to given value.";
    }
    
}
