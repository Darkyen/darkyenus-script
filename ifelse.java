import darkyenus.dipt.Command;
import darkyenus.dipt.DiptMain;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 11:13 AM
 */
@SuppressWarnings("UnusedDeclaration")
public class ifelse implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            if(arguments[0].equalsIgnoreCase("true")){
                DiptMain.ExecutionFrame frame = new DiptMain.ExecutionFrame(arguments[1]);
                return frame.getResult(reference);
            } else if(arguments[0].equalsIgnoreCase("false") && arguments.length >= 3){
                DiptMain.ExecutionFrame frame = new DiptMain.ExecutionFrame(arguments[2]);
                return frame.getResult(reference);
            }
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "if <condition> <command if true> [command if false]\n" +
                "Standard if function, first command is executed if true and second, optional, if false.\n" +
                "In cases when condition is not true, nor false, nothing is executed and null is returned.\n" +
                "Returns the result of executed command.";
    }
}
