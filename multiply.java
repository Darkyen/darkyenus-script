
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class multiply implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int result = Integer.parseInt(arguments[0]);
            for (int i = 1; i < arguments.length; i++) {
                String value = arguments[i];
                result *= Integer.parseInt(value);
            }
            return Integer.toString(result);
        } catch (NumberFormatException numberFormatException) {
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "multiply <arguments>\nMultiplies all following numbers together. Returns null if non number found.";
    }
    
}
