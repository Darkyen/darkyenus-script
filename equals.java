import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:42 AM
 */
public class equals implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        String lastArgument = null;
        for(String argument:arguments){
            if(lastArgument == null){
                lastArgument = argument;
            }else{
                if(!lastArgument.equals(argument)){
                    return "false";
                }
            }
        }
        return "true";
    }

    @Override
    public String getHelp() {
        return "equals [... parameters]\n" +
                "If all parameters are same, returns true, otherwise false.";
    }
}
