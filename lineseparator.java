import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 8:14 AM
 */
public class lineseparator implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length >= 1 && arguments[0].equalsIgnoreCase("real")){
            return System.getProperty("line.separator","null");
        }else{
            return "\n";
        }
    }

    @Override
    public String getHelp() {
        return "lineseparator [real]\n" +
                "Returns \\n character.\n" +
                "If [real] flag is set, returns real system line separator, which may be different.\n" +
                "This is because Dipt internally uses \\n as line separator, mainly for arrays and line reading.";
    }
}
