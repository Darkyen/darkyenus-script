import darkyenus.dipt.Command;
import darkyenus.dipt.DiptMain;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 10:21 AM
 */
public class execute implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        for(String command:arguments){
            DiptMain.ExecutionFrame frame = new DiptMain.ExecutionFrame(command);
            frame.getResult(reference);
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "execute []<command>\n" +
                "Execute more than one command in one command. Useful encapsulating command.";
    }
}
