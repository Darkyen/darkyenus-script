package darkyenus.dipt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Darkyen
 */
public class Reference implements DiptListener{
    private CommandLoader loader;
    private File workingDirectory;
    private HashMap<String,String> variables = new HashMap<String, String>();
    private ArrayList<DiptListener> listeners = new ArrayList<DiptListener>();
    private HashMap<String,String> aliases = new HashMap<String, String>();
    
    public Reference(File workingDirectory){
        this.loader = new CommandLoader(this);
        this.workingDirectory = workingDirectory;
    }

    public void addListener(DiptListener listener){
        listeners.add(listener);
    }
    
    /**
     * @return the workingDirectory
     */
    public File getWorkingDirectory() {
        return workingDirectory;
    }

    /**
     * @param workingDirectory the workingDirectory to set
     */
    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }
    
    public void setVariable(String variable,String value){
        variables.put(variable.toLowerCase(), value);
    }
    
    public String getVariable(String variable){
        return variables.get(variable.toLowerCase());
    }
    
    public Command getCommand(String command){
        return loader.getCommand(command);
    }

    public String getAlias(String alias){
        return aliases.get(alias);
    }

    public void addAlias(String alias,String command){
        aliases.put(alias,command);
    }

    public void removeAlias(String alias){
        aliases.remove(alias);
    }
    
    public CommandLoader getLoader(){
        return loader;
    }

    @Override
    public void onCommandRegistered(Command command) {
        for(DiptListener listener:listeners){
            listener.onCommandRegistered(command);
        }
    }

    @Override
    public void onShutdown() {
        for(DiptListener listener:listeners){
            listener.onShutdown();
        }
    }
}
