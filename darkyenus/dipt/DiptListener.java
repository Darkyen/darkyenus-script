package darkyenus.dipt;

/**
 *
 * @author Darkyen
 */
public interface DiptListener {
    public void onCommandRegistered(Command command);
    public void onShutdown();
}
