package darkyenus.dipt;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Darkyen
 */
public class CommandLoader extends ClassLoader{
    
    private HashMap<String,Command> commands = new HashMap<String, Command>();
    private HashSet<File> alreadyLoadedJars = new HashSet<File>();
    private HashSet<URLClassLoader> classLoaders = new HashSet<URLClassLoader>();
    private Reference reference;
    
    public CommandLoader(Reference reference){
        super();
        this.reference = reference;
    }
    
    public Command getCommand(String command){
        if(commands.containsKey(command)){
            return commands.get(command);
        }else{
            try {
                Class<?> newCommandClass = null;
                try {
                    newCommandClass = this.loadClass(command);
                } catch (ClassNotFoundException classNotFoundException) {
                    for(URLClassLoader loader:classLoaders){
                        try {
                            newCommandClass = loader.loadClass(command);
                        } catch (Exception e) {}
                    }
                }
                
                if(newCommandClass == null){
                    return null;
                }
                
                if(Command.class.isAssignableFrom(newCommandClass)){
                    Command commandObject = (Command) newCommandClass.newInstance();
                    commands.put(command, commandObject);
                    return commandObject;
                }else{
                    System.out.println("Class "+newCommandClass.getName()+" is not a Command!");
                    return null;
                }
            } catch (Exception ex) {
                Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }
    
    public boolean loadLibrary(String jar){
        try {
            String jarPath = jar;
            if(!jarPath.toLowerCase().endsWith(".jar")){
                jarPath = jarPath+".jar";
            }
            File fileToLoad = new File(jarPath);
            if(!fileToLoad.canRead()){
                fileToLoad = new File(reference.getWorkingDirectory(), jarPath);
                if(!fileToLoad.canRead()){
                    System.out.println("File \""+jarPath+"\" not found.");
                    return false;
                }
            }
            
            if(alreadyLoadedJars.contains(fileToLoad)){
                return true;
            }else{
                alreadyLoadedJars.add(fileToLoad);
                URLClassLoader loader = new URLClassLoader(new URL[]{fileToLoad.toURI().toURL()}, this);
                classLoaders.add(loader);
                return true;
            }
        } catch (Exception ex) {
            System.out.println(jar+" broke. "+ex);
            return false;
        }
    }
}
