package darkyenus.dipt;

/**
 *
 * @author Darkyen
 */
public interface Command {
    public String execute(Reference reference, String[] arguments);
    public String getHelp();
}
