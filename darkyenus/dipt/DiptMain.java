package darkyenus.dipt;

import darkyenus.utils.StringUtil;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Main class for Dipt.
 *
 * Private property.
 * @author Darkyen
 */
public class DiptMain {

    public static final byte MAJOR = 1;
    public static final byte MINOR = 1;

    private static Reference reference;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        reference = new Reference(new File(System.getProperty("user.dir")));
        process(args);
        reference.onShutdown();
        System.exit(0);
    }
    
    private static void process(String[] args){
        long timeStarted = System.currentTimeMillis();
        if(args.length == 0){
            System.out.println("Darkyenus Script - Dipt - "+MAJOR+"."+MINOR);
            System.out.println("Possible arguments:");
            System.out.println(" -e To execute following text");
            System.out.println(" -l To enter live execution mode");
            System.out.println(" Path - absolute path to file to execute");
        }else{
            if(args[0].startsWith("-")){
                if(args[0].startsWith("-e")){
                    //Execute following
                    String[] newArgs = new String[args.length-1];
                    System.arraycopy(args, 1, newArgs, 0, newArgs.length);
                    ExecutionFrame frame = new ExecutionFrame(newArgs);
                    frame.getResult(reference);
                }else if(args[0].startsWith("-l")){
                    System.out.println("Starting live execution mode, type \"exit\" to exit. To turn on automatic result echoing, type \"toggleecho\".");
                    Scanner scanner = new Scanner(System.in);
                    boolean echo = false;
                    while(true){
                        System.out.print("> ");
                        String read = scanner.nextLine();
                        if(read.equalsIgnoreCase("exit")){
                            break;
                        } else if (read.equalsIgnoreCase("toggleecho")){
                            echo = !echo;
                            if(echo){
                                System.out.println("Echoing results.");
                            }else{
                                System.out.println("No longer echoing results.");
                            }
                        } else {
                            ExecutionFrame frame = new ExecutionFrame(read);
                            if(echo){
                                System.out.println(frame.getResult(reference));
                            }else{
                                frame.getResult(reference);
                            }
                        }
                    }
                }else{
                    System.out.println("Invalid arguments: "+ Arrays.toString(args));
                    process(new String[0]);
                }
            }else{
                StringBuilder absolutePath = new StringBuilder();
                absolutePath.append(args[0]);
                for(int i=1;i<args.length;i++){
                    absolutePath.append(" ");
                    absolutePath.append(args[i]);
                }
                try {
                    File commandFile = new File(absolutePath.toString());
                    if(commandFile.canRead()){
                        BufferedReader commandInputStream = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(commandFile)), "UTF-8"));
                        String command = commandInputStream.readLine();
                        while(command != null){
                            if (!command.startsWith("//") && !command.startsWith("#")) {
                                ExecutionFrame commandFrame = new ExecutionFrame(command);
                                commandFrame.getResult(reference);
                            }
                            command = commandInputStream.readLine();
                        }
                        long timeTook = System.currentTimeMillis()-timeStarted;
                        int secondsTook = (int) (timeTook/1000);
                        System.out.println("Done in "+secondsTook+" seconds.");
                    }else{
                        System.out.println("Could not read the file: "+absolutePath.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }
        }
    }

    private interface Frame {

        public String getResult(Reference reference);
    }

    public static class ExecutionFrame implements Frame {

        public ExecutionFrame(String string) {
            this(string.split(" "));
        }

        public ExecutionFrame(String[] args){
            if(args.length > 0){
                command = args[0];
                String[] parameters = StringUtil.splitContextAware(args,1," ",'(',')',false);
                for(String parameter:parameters){
                    if(parameter.startsWith("\"") && parameter.endsWith("\"")){
                        arguments.add(new ArgumentFrame(parameter.substring(1,parameter.length()-1)));
                    }else{
                        arguments.add(new ExecutionFrame(parameter));
                    }
                }
            }//else empty line
        }
        
        private String command;
        private ArrayList<Frame> arguments = new ArrayList<Frame>();

        @Override
        public String getResult(Reference reference) {
            if (command == null || command.isEmpty() || command.equalsIgnoreCase("null")) {
                return "null";
            } else {
                String aliasedCommand = reference.getAlias(command);
                if (aliasedCommand == null) {
                    Command commandObj = reference.getCommand(command);
                    if (commandObj == null) {
                       if(command.equalsIgnoreCase("true")){//Not ignore case to allow overriding of some sorts
                           return "true";
                       }else if(command.equalsIgnoreCase("false")){
                           return "false";
                       }
                        try {
                            Integer.parseInt(command);
                            return command;//Command is a number (somehow)
                        } catch (Exception ex) {
                            System.out.println("Command \"" + command + "\" not found.");
                            return "null";
                        }
                    } else {
                        String[] evaluatedArguments = new String[arguments.size()];
                        for (int i = 0; i < evaluatedArguments.length; i++) {
                            evaluatedArguments[i] = arguments.get(i).getResult(reference);
                        }
                        return commandObj.execute(reference, evaluatedArguments);
                    }
                }else{
                    ExecutionFrame aliasFrame = new ExecutionFrame(aliasedCommand);
                    return aliasFrame.getResult(reference);
                }
            }
        }
    }

    private static class ArgumentFrame implements Frame {

        private String value;

        public ArgumentFrame(String value) {
            this.value = value;
        }

        @Override
        public String getResult(Reference reference) {
            if (value == null) {
                return "null";
            } else {
                return value;
            }
        }
    }
}
