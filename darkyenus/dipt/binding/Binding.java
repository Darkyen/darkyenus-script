package darkyenus.dipt.binding;

import darkyenus.dipt.DiptMain;
import darkyenus.dipt.Reference;

/**
* Private property.
* User: Darkyen
* Date: 3/14/13
* Time: 8:37 PM
*/
public abstract class Binding {

    private DiptMain.ExecutionFrame frame;
    private Reference reference;

    public Binding(Reference reference, String command){
        frame = new DiptMain.ExecutionFrame(command);
        this.reference = reference;
    }

    public void receiveEvent(BindManager.InputSnapshot snapshot){
        if(isValid(snapshot)){
            frame.getResult(reference);
        }
    }

    public abstract boolean isValid(BindManager.InputSnapshot snapshot);
}
