package darkyenus.dipt.binding;

import darkyenus.utils.collections.LinkedCartCollection;
import darkyenus.dipt.Command;
import darkyenus.dipt.DiptListener;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseWheelEvent;
import org.jnativehook.mouse.NativeMouseWheelListener;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Darkyen
 */
@SuppressWarnings("UnusedDeclaration")
public class BindManager implements NativeKeyListener, NativeMouseListener, NativeMouseWheelListener, DiptListener {
    
    
    public static final BindManager manager = new BindManager();
    private boolean started = false;
    
    public void start(){
        try {
            if (!GlobalScreen.isNativeHookRegistered()) {
                GlobalScreen.registerNativeHook();
                GlobalScreen.getInstance().addNativeKeyListener(manager);
                GlobalScreen.getInstance().addNativeMouseListener(manager);
                GlobalScreen.getInstance().addNativeMouseWheelListener(manager);
                System.out.println("Bind manager: Started.");
            }else{
                System.out.println("Bind manager: Already running.");
            }
        } catch (NativeHookException ex) {
            System.out.println("Native hook could not be registered. "+ex.getMessage());
        }
    }
    
    public void stop(){
        if (GlobalScreen.isNativeHookRegistered()) {
            GlobalScreen.getInstance().removeNativeKeyListener(manager);
            GlobalScreen.getInstance().removeNativeMouseListener(manager);
            GlobalScreen.getInstance().removeNativeMouseWheelListener(manager);
            GlobalScreen.unregisterNativeHook();
            System.out.println("Bind manager: Stopped.");
        }else{
            System.out.println("Bind manager: Not running.");
        }
    }

    private void ensureStarted(){
        if(!started){
            start();
            started = true;
        }
    }
    
    private boolean echoMode = false;

    private HashMap<Integer,Binding> bindings = new HashMap<Integer, Binding>();
    private int lastBindID = 0;
    private InputSnapshot inputSnapshot = new InputSnapshot();
    private LinkedCartCollection<BindManagerDelegate> delegates = new LinkedCartCollection<BindManagerDelegate>();
    
    public void setEchoMode(boolean echoMode){
        this.echoMode = echoMode;
    }
    
    public synchronized int bind(Binding binding){
        ensureStarted();
        lastBindID++;
        bindings.put(lastBindID, binding);
        return lastBindID;
    }

    public synchronized boolean unbind(int id){
        return bindings.remove(id) != null;
    }

    public synchronized void unbindAll(){
        bindings.clear();
    }

    public synchronized void addDelegate(BindManagerDelegate delegate){
        ensureStarted();
        delegates.add(delegate);
    }

    @Override
    public synchronized void nativeKeyPressed(NativeKeyEvent nke) {
        if(echoMode){
            System.out.println(nke.paramString());
        }
        inputSnapshot.pressedKeys.add(nke.getKeyCode());
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeKeyPressed(nke);
            }
        }
        sendInputEvent();
    }

    @Override
    public synchronized void nativeKeyReleased(NativeKeyEvent nke) {
        if(echoMode){
            System.out.println(nke.paramString());
        }
        inputSnapshot.pressedKeys.remove(nke.getKeyCode());
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeKeyReleased(nke);
            }
        }
        sendInputEvent();
    }

    @Override
    public synchronized void nativeKeyTyped(NativeKeyEvent nke) {
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeKeyTyped(nke);
            }
        }
    }

    @Override
    public synchronized void nativeMouseClicked(NativeMouseEvent nmc) {
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeMouseClicked(nmc);
            }
        }
    }

    @Override
    public synchronized void nativeMousePressed(NativeMouseEvent nme) {
        if(echoMode){
            System.out.println(nme.paramString());
        }
        inputSnapshot.pressedMouseButtons.add(nme.getButton());
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeMousePressed(nme);
            }
        }
        sendInputEvent();
    }

    @Override
    public synchronized void nativeMouseReleased(NativeMouseEvent nme) {
        if(echoMode){
            System.out.println(nme.paramString());
        }
        inputSnapshot.pressedMouseButtons.remove(nme.getButton());
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeMouseReleased(nme);
            }
        }
        sendInputEvent();
    }

    @Override
    public synchronized void nativeMouseWheelMoved(NativeMouseWheelEvent nmwe) {
        if(echoMode){
            System.out.println(nmwe.paramString());
        }
        if (nmwe.getScrollType() == NativeMouseWheelEvent.WHEEL_UNIT_SCROLL) {
            inputSnapshot.lastMouseWheelMovement = nmwe.getScrollAmount();
            inputSnapshot.hasWheelEvent = true;
            sendInputEvent();
            inputSnapshot.hasWheelEvent = false;
        }
        for(LinkedCartCollection.Cart<BindManagerDelegate> delegateCart = delegates.getFirst();delegateCart != null;delegateCart = delegateCart.getNext()){
            if(delegateCart.getEntry().canBeRemoved()){
                delegateCart.remove();
            }else{
                delegateCart.getEntry().nativeMouseWheelMoved(nmwe);
            }
        }
    }

    private void sendInputEvent(){
        for(Binding binding:bindings.values()){
            binding.receiveEvent(inputSnapshot);
        }
    }
    
    @Override
    public void onCommandRegistered(Command command){}
    
    @Override
    public void onShutdown(){
        stop();
    }

    public static class InputSnapshot{
        private HashSet<Integer> pressedKeys = new HashSet<Integer>();
        private HashSet<Integer> pressedMouseButtons = new HashSet<Integer>();
        private boolean hasWheelEvent = false;
        private int lastMouseWheelMovement = 0;

        public boolean isKeyPressed(int key){
            return pressedKeys.contains(key);
        }

        public boolean isMouseButtonPressed(int key){
            return pressedMouseButtons.contains(key);
        }

        public boolean isWheelEvent(){
            return hasWheelEvent;
        }

        public int getLastMouseWheelMovement(){
            return lastMouseWheelMovement;
        }
    }

}
