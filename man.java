
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class man implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length != 1){
            System.out.println("Invalid amount of arguments.");
            return "null";
        }else{
            Command command = reference.getCommand(arguments[0]);
            if (command != null) {
                System.out.println(command.getHelp());
                return command.getHelp();
            }else{
                System.out.println("Command \""+arguments[0]+"\" not found.");
                return "null";
            }
        }
    }

    @Override
    public String getHelp() {
        return "man <command>\nPrints info about command.";
    }
    
}
