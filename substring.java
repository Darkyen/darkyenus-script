import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 8:00 AM
 */
public class substring implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length >= 2){
            try {
                int beginIndex = Integer.parseInt(arguments[1]);
                int endIndex = arguments.length >= 3?Integer.parseInt(arguments[2]):arguments[0].length();
                return arguments[0].substring(beginIndex, endIndex);
            } catch (Exception ex) {
                System.out.println("substring: An error occurred. "+ex);
                return "null";
            }
        }else{
            System.out.println("Invalid usage of substring.");
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "substring <string> <begin index> [end index]\n" +
                "Works in same way as java.lang.String.substring().\n" +
                "Returns part of the given string. Begin index is how much characters will be cut from front.\n" +
                "Optional end index is on which index will cutting start again.\n" +
                "Default value of end index is length of string.";
    }
}
