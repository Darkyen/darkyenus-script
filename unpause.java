import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 11:12 AM
 */
public class unpause implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        synchronized (pause.LOCK){
            pause.LOCK.notifyAll();
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "unpause\n" +
                "Unpauses all paused threads.";
    }
}
