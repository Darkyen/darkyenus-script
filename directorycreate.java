import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.io.File;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:58 AM
 */
public class directorycreate implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            File pathAnalyzer = new File(arguments[0]);
            File newFile;
            if(pathAnalyzer.isAbsolute()){
                newFile = pathAnalyzer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }

            return Boolean.toString(newFile.mkdirs());
        } catch (Exception ex) {
            if (arguments.length < 1 || !arguments[0].equalsIgnoreCase("mute")) {
                System.out.println("Could not read file: "+ex);
            }
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "directorycreate <path to directory> [mute]\n" +
                "Creates given directory.\n" +
                "Specify [mute] flag to not print potential errors.\n" +
                "Returns true if creation was successful.";
    }
}
