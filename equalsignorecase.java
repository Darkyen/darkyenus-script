import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:45 AM
 */
public class equalsignorecase implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        String lastArgument = null;
        for(String argument:arguments){
            if(lastArgument == null){
                lastArgument = argument;
            }else{
                if(!lastArgument.equalsIgnoreCase(argument)){
                    return "false";
                }
            }
        }
        return "true";
    }

    @Override
    public String getHelp() {
        return "equalsignorecase [... parameters]\n" +
                "If all parameters are same, ignoring case, returns true, otherwise false.";
    }
}
