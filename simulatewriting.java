import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 8:11 AM
 */
@SuppressWarnings("UnusedDeclaration")
public class simulatewriting implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int[] chars = new int[arguments.length];
            for (int i = 0; i < chars.length; i++) {
                chars[i] = Integer.parseInt(arguments[i]);
            }

            boolean waits = true;
            if(arguments.length >= 2 && arguments[1].equalsIgnoreCase("instant")){
                waits = false;
            }

            for(int letter:chars){
                if(letter < 0){
                    simulate.getRobot().keyRelease(-letter);
                    if(waits){
                        simulate.getRobot().waitForIdle();
                    }
                }else{
                    simulate.getRobot().keyPress(letter);
                    if(waits){
                        simulate.getRobot().waitForIdle();
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "simulatewriting <text> [instant]\n" +
                "Will simulate writing of given text. Specify instant flag to not wait for completion.";
    }
}
