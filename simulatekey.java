import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/14/13
 * Time: 10:05 PM
 */
public class simulatekey implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int keyToSimulate = Integer.parseInt(arguments[0]);
            int timeToKeep = 0;
            if(arguments.length >= 2){
                timeToKeep = Integer.parseInt(arguments[1]);
            }
            boolean blocking = true;
            if(arguments.length >= 3){
                blocking = arguments[2].equalsIgnoreCase("true");
            }

            final int finalKeyToSimulate = keyToSimulate;
            final int finalTimeToKeep = timeToKeep;

            Runnable presser = new Runnable() {
                @Override
                public void run() {
                    try {
                        simulate.getRobot().keyPress(finalKeyToSimulate);
                        long started = System.currentTimeMillis();
                        simulate.getRobot().waitForIdle();
                        long timeToWait = finalTimeToKeep - (System.currentTimeMillis()-started);
                        if(timeToWait > 0){
                            Thread.sleep(timeToWait);
                        }
                        simulate.getRobot().keyRelease(finalKeyToSimulate);
                    } catch (Exception e) {
                        System.out.println("Failed to simulate: "+e);
                    }
                }
            };

            if(blocking){
                presser.run();
            }else{
                Thread presserThread = new Thread(presser);
                presserThread.setDaemon(false);
                presserThread.setName("DiptBinding: Key pressing thread");
                presserThread.start();
            }
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "simulatekey <key> [duration] [blocking]\n" +
                "This will try to simulate key press in host environment.\n" +
                "   key - Java key code; see \"http://docs.oracle.com/javase/6/docs/api/constant-values.html#java.awt.event.KeyEvent.VK_0\"\n" +
                "   duration - key press duration in milliseconds (1s = 1000ms), default is as fast as possible, result can't be guaranteed\n" +
                "   blocking - \"true\" or \"false\", whether this simulation should block execution until complete, defaults to yes, turning this off may cause unexpected behavior\n" +
                "Returns null.";
    }
}
