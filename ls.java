
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Darkyen
 */
public class ls implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        boolean showHidden = false;
        boolean returnSpaceSeparated = false;
        boolean noEcho = false;
        boolean files = true;
        boolean directories = true;
        if (arguments.length == 1) {
            String arg = arguments[0].toLowerCase();
            showHidden = arg.contains("a");
            returnSpaceSeparated = arg.contains("l");
            noEcho = arg.contains("e");
            if (arg.contains("f")) {
                directories = false;
            }
            if (arg.contains("d")) {
                files = false;
            }
        }
        ArrayList<String> passedFilenames = new ArrayList<String>();
        for (File mayPass : reference.getWorkingDirectory().listFiles()) {
            if ((mayPass.isHidden() && showHidden) || !mayPass.isHidden()) {
                if ((mayPass.isDirectory() && directories) || !mayPass.isDirectory()) {
                    if ((mayPass.isFile() && files) || !mayPass.isFile()) {
                        passedFilenames.add(mayPass.getName() + (mayPass.isDirectory() ? "/" : ""));
                    }
                }
            }
        }
        StringBuilder result = new StringBuilder();
        if (!passedFilenames.isEmpty()) {
            Iterator<String> passedFilenamesIterator = passedFilenames.iterator();
            result.append(passedFilenamesIterator.next());
            while(passedFilenamesIterator.hasNext()){
                if(returnSpaceSeparated){
                    result.append(" ");
                }else{
                    result.append("\n");
                }
                result.append(passedFilenamesIterator.next());
            }
        }

        if (!noEcho) {
            System.out.println(result.toString());
        }
        return result.toString();
    }

    @Override
    public String getHelp() {
        return "ls [aledf]\nReturns all files in working directory. Argument string may include \"a\" to show hidden files, \"L\" to return space separated list (names with spaces are encomapssed) rather than one result per line, \"e\" to suppress echo which this command normally creates, \"d\" to list only directories and \"f\" to list only files.";
    }
}
