
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class pwd implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        String result = reference.getWorkingDirectory().getAbsolutePath();
        if(!(arguments.length != 0 && arguments[0].equalsIgnoreCase("e"))){
            System.out.println(result);
        }
        return result;
    }

    @Override
    public String getHelp() {
       return "pwd [e]\nReturns working directory. Add \"e\" to suppress echo, similar to \"ls\" command.";
    }
    
}
