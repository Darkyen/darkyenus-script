import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/27/13
 * Time: 2:54 PM
 */
public class subtract implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int result = Integer.parseInt(arguments[0]);
            for (int i = 1; i < arguments.length; i++) {
                String value = arguments[i];
                result -= Integer.parseInt(value);
            }
            return Integer.toString(result);
        } catch (Exception numberFormatException) {
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "subtract <arguments>\nSubtracts second argument from first, then third etc. Returns null if non number found.\n" +
                "Tip: You can use add command with negative numbers too.";
    }
}
