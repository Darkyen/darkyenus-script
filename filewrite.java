import darkyenus.utils.StringUtil;
import darkyenus.utils.files.Writer;
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.io.File;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:57 AM
 */
public class filewrite implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        boolean hasMuteFlag = arguments.length >= 2 && arguments[1].equalsIgnoreCase("-mute");
        try {
            File pathAnalizer = new File(arguments[0]);
            File newFile;
            if(pathAnalizer.isAbsolute()){
                newFile = pathAnalizer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }

            String toWrite = StringUtil.join(arguments,"\n",hasMuteFlag?2:1);
            Writer writer = new Writer(newFile);
            writer.write(toWrite);
            return "null";
        } catch (Exception ex) {
            if (!hasMuteFlag) {
                System.out.println("Could not read file: "+ex);
            }
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "filewrite <path to file> [-mute] <... content>\n" +
                "Writes content (as lines) into specified file.\n" +
                "Specify [mute] flag to not print potential errors.";
    }
}
