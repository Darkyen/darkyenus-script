import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.io.File;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:58 AM
 */
public class directorydelete implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            File pathAnalyzer = new File(arguments[0]);
            File newFile;
            if(pathAnalyzer.isAbsolute()){
                newFile = pathAnalyzer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }

            if(newFile.isDirectory()){
                deleteFolder(newFile);
                if(newFile.isDirectory()){
                    return "false";
                }else{
                    return "true";
                }
            }else{
                return "false";
            }
        } catch (Exception ex) {
            if (arguments.length < 1 || !arguments[0].equalsIgnoreCase("mute")) {
                System.out.println("Could not read file: "+ex);
            }
            return "null";
        }
    }

    private void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    @Override
    public String getHelp() {
        return "directorydelete <path to directory> [mute]\n" +
                "Deletes given directory and all its content.\n" +
                "Specify [mute] flag to not print potential errors.\n" +
                "Returns true if removal was successful.";
    }
}
