import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/27/13
 * Time: 2:59 PM
 */
public class setclipboard implements Command{
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(arguments[0]),null);
        } catch (Exception ex) {
            if (!(arguments.length >= 2 && arguments[0].equalsIgnoreCase("mute"))) {
                System.out.println("Could not set clipboard: "+ex);
            }
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "setclipboard <data> [mute]\n" +
                "Sets system clipboard, string flavour.\n" +
                "Add 'mute' parameter to not print potential errors.";
    }
}
