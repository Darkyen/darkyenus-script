
import darkyenus.dipt.Command;
import darkyenus.dipt.DiptMain;
import darkyenus.dipt.Reference;

import java.io.File;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:59 AM
 */
public class forfiles implements Command {

    private static final String VAR = "forfilesfile";

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            File pathAnalyzer = new File(arguments[0]);
            File newFile;
            if(pathAnalyzer.isAbsolute()){
                newFile = pathAnalyzer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }

            if(newFile.isDirectory()){
                String[] files = newFile.list();
                if(files != null){
                    for (String file:files){
                        reference.setVariable(VAR,file);
                        DiptMain.ExecutionFrame frame = new DiptMain.ExecutionFrame(arguments[1]);
                        frame.getResult(reference);
                    }
                }
            }
            return "null";
        } catch (Exception ex) {
            if (arguments.length < 2 || !arguments[1].equalsIgnoreCase("mute")) {
                System.out.println("Could not read file: "+ex);
            }
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "forfiles <path to directory> <command> [mute]\n" +
                "Calls <command> for every directory in tree.\n" +
                "Name of currently processed file is stored in variable '"+VAR+"'\n" +
                "Specify [mute] flag to not print potential errors.";
    }
}
