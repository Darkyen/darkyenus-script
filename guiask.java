import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import javax.swing.*;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 8:54 AM
 */
public class guiask implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            String content = arguments[0];
            String title = arguments.length >= 2?arguments[1]:"";
            int type = JOptionPane.PLAIN_MESSAGE;
            if(arguments.length >= 3){
                if(arguments[2].equalsIgnoreCase("plain")){
                    type = JOptionPane.PLAIN_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("error")){
                    type = JOptionPane.ERROR_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("warning")){
                    type = JOptionPane.WARNING_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("info")){
                    type = JOptionPane.INFORMATION_MESSAGE;
                }else if(arguments[2].equalsIgnoreCase("question")){
                    type = JOptionPane.QUESTION_MESSAGE;
                }
            }

            String result = JOptionPane.showInputDialog(null, content,title,type);
            if(result != null){
                return result;
            }
        } catch (Exception ex) {
            System.out.println("Could not show message: "+ex);
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "guiask <content> [title] [type]\n" +
                "Shows an input window with given message and properties.\n" +
                "Type alters layout/icon and can be one of these:\n" +
                "plain (default), error, warning, info, question\n" +
                "Command returns input given by user or null.";
    }
}
