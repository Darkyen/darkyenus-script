
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;
import java.util.Scanner;
import java.util.regex.Pattern;


/**
 *
 * @author Darkyen
 */
public class input implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        Scanner inputScanner = new Scanner(System.in);
        String input = inputScanner.nextLine();
        if(arguments.length == 1){
            Pattern pattern = Pattern.compile(arguments[0]);
            StringBuilder result = new StringBuilder();
            for(char c:input.toCharArray()){
                if(pattern.matcher(""+c).matches()){
                    result.append(c);
                }
            }
            input = result.toString();
        }
        return input;
    }

    @Override
    public String getHelp() {
        return "input [regex]\nWaits for user to input some text and press enter. If regex argument is present, all characters that don't match are removed.";
    }
    
}
