import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 8:07 AM
 */
public class length implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        int count = 0;
        for(String arg:arguments){
            count += arg.length();
        }
        return Integer.toString(count);
    }

    @Override
    public String getHelp() {
        return "length [... string]\n" +
                "Returns amount of characters from which given string(s) consist.";
    }
}
