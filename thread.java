import darkyenus.dipt.Command;
import darkyenus.dipt.DiptMain;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 10:29 AM
 */
public class thread implements Command {

    private static int lastId = 1;

    @Override
    public String execute(final Reference reference, String[] arguments) {
        try {
            final DiptMain.ExecutionFrame frame = new DiptMain.ExecutionFrame(arguments[0]);
            boolean daemon = false;
            if(arguments.length >= 2){
                if("daemon".equalsIgnoreCase(arguments[1])){
                    daemon = true;
                }
            }
            Thread thread = new Thread(){
                public void run(){
                    frame.getResult(reference);
                }
            };
            thread.setDaemon(daemon);
            thread.setName("Dipt Thread "+lastId);
            lastId++;
            thread.start();
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "thread <command> [daemon]\n" +
                "This will execute given command in new thread. You can add \"daemon\" flag to make thread daemon.";
    }
}
