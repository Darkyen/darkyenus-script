import darkyenus.dipt.Command;
import darkyenus.dipt.DiptMain;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 12:01 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class whiledo implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            DiptMain.ExecutionFrame conditionFrame = new DiptMain.ExecutionFrame(arguments[0]);
            DiptMain.ExecutionFrame commandFrame = new DiptMain.ExecutionFrame(arguments[1]);
            String lastResult = "null";
            while("true".equalsIgnoreCase(conditionFrame.getResult(reference))){
                lastResult = commandFrame.getResult(reference);
            }
            return lastResult;
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "whiledo <condition> <command>\n" +
                "Will keep executing command while condition is true.\n" +
                "NOTE: Both must be in textual form, not in command form.\n" +
                "Will return last value returned by command.";
    }
}
