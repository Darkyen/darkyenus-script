
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class echo implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        for(String string:arguments){
            System.out.println(string);
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "echo <arguments>\nPrints arguments to output, one argument per line.";
    }
    
}
