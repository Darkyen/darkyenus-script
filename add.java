
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class add implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int result = 0;
            for(String value:arguments){
                result += Integer.parseInt(value);
            }
            return Integer.toString(result);
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder();
            for(String value:arguments){
                builder.append(value);
            }
            return builder.toString();
        }
    }

    @Override
    public String getHelp() {
        return "add <arguments>\n" +
                "Adds integers or strings together.";
    }
    
}
