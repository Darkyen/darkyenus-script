
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class modulo implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int result = Integer.parseInt(arguments[0]);
            for (int i = 1; i < arguments.length; i++) {
                String value = arguments[i];
                result %= Integer.parseInt(value);
            }
            return Integer.toString(result);
        } catch (NumberFormatException numberFormatException) {
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "modulo <arguments>\nSimilar to divide, but with modulo operator.";
    }
    
}
