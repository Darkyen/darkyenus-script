import darkyenus.dipt.binding.BindManager;
import darkyenus.dipt.binding.BindManagerDelegate;
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;
import org.jnativehook.keyboard.NativeKeyEvent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * Private property.
 * User: Darkyen
 * Date: 3/15/13
 * Time: 8:32 AM
 */
@SuppressWarnings("ALL")
public class retrievekeycode implements Command {
    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            int delay = 1;
            if(arguments.length >= 2){
                delay = Integer.parseInt(arguments[1]);
                if(delay < 0){
                    delay = 0;
                }
            }

            if(arguments[0].equalsIgnoreCase("bind")){
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                final IntBuffer buffer = IntBuffer.allocate(1);
                BindManagerDelegate delegate = new BindManagerDelegate() {
                    @Override
                    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
                        buffer.put(nativeKeyEvent.getKeyCode());
                        countDownLatch.countDown();
                        this.finish();
                    }
                };
                BindManager.manager.addDelegate(delegate);
                countDownLatch.await();
                buffer.flip();
                return Integer.toString(buffer.get());
            }else if(arguments[0].equalsIgnoreCase("simulate")){
                final ArrayList<Integer> codes = new ArrayList<Integer>();
                final JFrame catcher = new JFrame("Got: 0");
                final JButton closer = new JButton("Done");
                final long fromTimeStamp = System.currentTimeMillis()+(delay*1000);
                final CountDownLatch countDownLatch = new CountDownLatch(1);

                catcher.add(closer);
                catcher.addKeyListener(new KeyListener() {
                    public void keyPressed(KeyEvent event) {
                        if (System.currentTimeMillis() > fromTimeStamp) {
                            codes.add(event.getKeyCode());
                            catcher.setTitle("Got: " + codes.size());
                        }
                    }

                    public void keyReleased(KeyEvent event) {
                        if (System.currentTimeMillis() > fromTimeStamp) {
                            codes.add(-event.getKeyCode());
                            catcher.setTitle("Got: " + codes.size());
                        }
                    }

                    public void keyTyped(KeyEvent event) {
                    }
                });
                catcher.setFocusable(true);
                closer.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        catcher.setVisible(false);
                        countDownLatch.countDown();
                    }
                });
                catcher.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                catcher.setVisible(true);
                catcher.requestFocus();
                catcher.requestFocusInWindow();
                countDownLatch.await();
                if(!codes.isEmpty()){
                    StringBuilder result = new StringBuilder();
                    result.append(codes.remove(0));
                    for(Integer moreCode:codes){
                        result.append(" ");
                        result.append(moreCode);
                    }
                    return result.toString();
                }else{
                    return "null";
                }
            }else throw new Exception();
        } catch (Exception ex) {
            System.out.println(getHelp());
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "retrievekeycode <bind | simulate> [delay]\n" +
                "Used for retrieving key codes to use in \"bind\" commands and in \"simulate\".\n" +
                "Note that those codes may not be the same!" +
                "   bind - waits for any key press and returns it as a number\n" +
                "   simulate - creates a window which must have focus to be able to catch keys, can catch multiple keys\n" +
                "You can specify delay argument to a positive number. Command will wait given amount of seconds before \n" +
                "catching anything. This is useful to assure that you are pressing the right key. Default delay is 1 second.";
    }
}
