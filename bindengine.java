
import darkyenus.dipt.binding.BindManager;
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
@SuppressWarnings("ALL")
public class bindengine implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length == 0){
            return getHelp();
        }else{
            if(arguments[0].equalsIgnoreCase("start")){
                BindManager.manager.start();
            }else if(arguments[0].equalsIgnoreCase("stop")){
                BindManager.manager.stop();
            }else if(arguments[0].equalsIgnoreCase("echoon")){
                BindManager.manager.setEchoMode(true);
            }else if(arguments[0].equalsIgnoreCase("echooff")){
                BindManager.manager.setEchoMode(false);
            }else{
                return getHelp();
            }
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "bindengine [start | stop | echoon | echooff]\nUtility commands for DiptBinding.";
    }
    
}
