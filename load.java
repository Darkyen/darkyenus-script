
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class load implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        boolean success = true;
        for(String argument:arguments){
            if(!reference.getLoader().loadLibrary(argument)){
                success = false;
            }
        }
        return success?"true":"false";
    }

    @Override
    public String getHelp() {
        return "load <path/s>\n" +
                "Tries to load a Dipt source jar(s) from given path. Returns \"true\" if all loaded properly, \"false\" otherwise.";
    }
}
