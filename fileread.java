import darkyenus.utils.StringUtil;
import darkyenus.utils.files.Reader;
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import java.io.File;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 7:57 AM
 */
public class fileread implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            File pathAnalizer = new File(arguments[0]);
            File newFile;
            if(pathAnalizer.isAbsolute()){
                newFile = pathAnalizer;
            }else{
                newFile = new File(reference.getWorkingDirectory(), arguments[0]);
            }

            Reader reader = new Reader(newFile);
            return StringUtil.join(reader.readRest(), "\n");
        } catch (Exception ex) {
            if (arguments.length < 1 || !arguments[0].equalsIgnoreCase("mute")) {
                System.out.println("Could not read file: "+ex);
            }
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "fileread <path to file> [mute]\n" +
                "Returns content of file or null if an error occurs.\n" +
                "Specify [mute] flag to not print potential errors.";
    }
}
