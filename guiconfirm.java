import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

import javax.swing.*;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/27/13
 * Time: 2:21 PM
 */
public class guiconfirm implements Command {

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            String content = arguments[0];
            String title = arguments.length >= 2?arguments[1]:"";
            int option = JOptionPane.YES_NO_OPTION;
            if(arguments.length >= 3){
                if(arguments[2].equalsIgnoreCase("yesno")){
                    option = JOptionPane.YES_NO_OPTION;
                }else if(arguments[2].equalsIgnoreCase("yesnocancel")){
                    option = JOptionPane.YES_NO_CANCEL_OPTION;
                }else if(arguments[2].equalsIgnoreCase("okcancel")){
                    option = JOptionPane.OK_CANCEL_OPTION;
                }
            }

            int type = JOptionPane.PLAIN_MESSAGE;
            if(arguments.length >= 4){
                if(arguments[3].equalsIgnoreCase("plain")){
                    type = JOptionPane.PLAIN_MESSAGE;
                }else if(arguments[3].equalsIgnoreCase("error")){
                    type = JOptionPane.ERROR_MESSAGE;
                }else if(arguments[3].equalsIgnoreCase("warning")){
                    type = JOptionPane.WARNING_MESSAGE;
                }else if(arguments[3].equalsIgnoreCase("info")){
                    type = JOptionPane.INFORMATION_MESSAGE;
                }else if(arguments[3].equalsIgnoreCase("question")){
                    type = JOptionPane.QUESTION_MESSAGE;
                }
            }

            int result = JOptionPane.showConfirmDialog(null,content,title,option,type);
            if(result == JOptionPane.OK_OPTION || result == JOptionPane.YES_OPTION){
                return "true";
            }else if(result == JOptionPane.NO_OPTION){
                return "false";
            }else{
                return "null";
            }
        } catch (Exception ex) {
            System.out.println("Could not show message: "+ex);
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "guiconfirm <content> [title] [option type] [message type]\n" +
                "Shows confirmation window, returns true, false or null.\n" +
                "Option type determines which buttons will be available. Possible values are yesno,yesnocancel and okcancel.\n" +
                "Ok and yes returns true, no returns false and other returns null.\n" +
                "Message type is same as in guimessage command (plain (default), error, warning, info, question)";
    }
}
