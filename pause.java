import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;

/**
 * Private property.
 * User: Darkyen
 * Date: 5/12/13
 * Time: 11:00 AM
 */
public class pause implements Command {

    public static final Object LOCK = new Object();

    @Override
    public String execute(Reference reference, String[] arguments) {
        if(arguments.length >= 1){
            try {
                int millis = Integer.parseInt(arguments[0]);
                synchronized (LOCK){
                    LOCK.wait(millis);
                }
            } catch (Exception ex) {
                System.out.println("pause: Not a valid number!");
            }
        }else{
            synchronized (LOCK){
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    System.out.println("pause: Interrupted: "+e);
                }
            }
        }
        return "null";
    }

    @Override
    public String getHelp() {
        return "pause [millis]\n" +
                "Pauses this thread for given amount of milliseconds or forever.\n" +
                "Can be unpaused by unpause command";
    }
}
