
import darkyenus.dipt.binding.BindManager;
import darkyenus.dipt.binding.Binding;
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class bind implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            Binding binding = null;
            if(arguments[0].equalsIgnoreCase("key") || arguments[0].equalsIgnoreCase("+key")){
                final int identifier = Integer.parseInt(arguments[1]);
                binding = new Binding(reference,arguments[2]) {

                    boolean keyPressed = false;

                    @Override
                    public boolean isValid(BindManager.InputSnapshot snapshot) {
                        if(snapshot.isKeyPressed(identifier)){
                            if(!keyPressed){
                                keyPressed = true;
                                return true;
                            }
                        }else{
                            keyPressed = false;
                        }
                        return false;
                    }
                };
            }else if(arguments[0].equalsIgnoreCase("-key")){
                final int identifier = Integer.parseInt(arguments[1]);
                binding = new Binding(reference,arguments[2]) {

                    boolean keyPressed = false;

                    @Override
                    public boolean isValid(BindManager.InputSnapshot snapshot) {
                        if(!snapshot.isKeyPressed(identifier)){
                            if(keyPressed){
                                keyPressed = false;
                                return true;
                            }
                        }else{
                            keyPressed = true;
                        }
                        return false;
                    }
                };
            }else if(arguments[0].equalsIgnoreCase("mouse") || arguments[0].equalsIgnoreCase("+mouse")){
                final int identifier = Integer.parseInt(arguments[1]);
                binding = new Binding(reference,arguments[2]) {

                    boolean buttonPressed = false;

                    @Override
                    public boolean isValid(BindManager.InputSnapshot snapshot) {
                        if(snapshot.isMouseButtonPressed(identifier)){
                            if(!buttonPressed){
                                buttonPressed = true;
                                return true;
                            }
                        }
                        return false;
                    }
                };
            }else if(arguments[0].equalsIgnoreCase("-mouse")){
                final int identifier = Integer.parseInt(arguments[1]);
                binding = new Binding(reference,arguments[2]) {

                    boolean buttonPressed = false;

                    @Override
                    public boolean isValid(BindManager.InputSnapshot snapshot) {
                        if(!snapshot.isMouseButtonPressed(identifier)){
                            if(buttonPressed){
                                buttonPressed = false;
                                return true;
                            }
                        }else{
                            buttonPressed = true;
                        }
                        return false;
                    }
                };
            }else if(arguments[0].equalsIgnoreCase("wheel")){
                final boolean positiveTrigger = "+".equals(arguments[1]);
                final boolean negativeTrigger = "-".equals(arguments[1]);
                int exactValueTemp = 0;
                if(!positiveTrigger && !negativeTrigger){
                    exactValueTemp = Integer.parseInt(arguments[1]);
                }
                final int exactValue = exactValueTemp;

                binding = new Binding(reference,arguments[2]) {

                    @Override
                    public boolean isValid(BindManager.InputSnapshot snapshot) {
                        if(snapshot.isWheelEvent()){
                            if(positiveTrigger){
                                return snapshot.getLastMouseWheelMovement() > 0;
                            }else if(negativeTrigger){
                                return snapshot.getLastMouseWheelMovement() < 0;
                            }else{
                                return snapshot.getLastMouseWheelMovement() == exactValue;
                            }
                        }
                        return false;
                    }
                };
            }
            int result = BindManager.manager.bind(binding);
            return Integer.toString(result);
        } catch (Exception ex) {
            System.out.println(getHelp());
            return "-1";
        }
    }

    @Override
    public String getHelp() {
        return "bind <key | mouse | wheel> <identifier> <command>\n" +
                "Binds given command to chosen user action.\n" +
                "Identifiers:\n" +
                "   key - Key id (can be seen when enabling \"bindengine echoon\"\n" +
                "   mouse - Mouse button id (see above; 1 = LMB, 2 = RMB, 3 = MMB, usually)\n" +
                "   wheel - Specified amount by which moved, or \"+\" for any positive amount and \"-\" for any negative.\n" +
                "Additional control:\n" +
                "   Binds will on default trigger when key or button is pressed (this doesn't apply to mouse wheel),\n" +
                "   however you can set \"-\" or \"+\" before key or mouse (ex. \"-mouse\") to explicitly specify when will\n" +
                "   event fire. Plus means on press and minus on release.\n" +
                "Returns id of newly bound action.";
    }
    
}
