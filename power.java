
import darkyenus.dipt.Command;
import darkyenus.dipt.Reference;


/**
 *
 * @author Darkyen
 */
public class power implements Command{

    @Override
    public String execute(Reference reference, String[] arguments) {
        try {
            if (arguments.length == 2) {
                return Integer.toString((int)Math.pow(Integer.parseInt(arguments[0]), Integer.parseInt(arguments[1])));
            } else {
                return "null";
            }
        } catch (NumberFormatException numberFormatException) {
            return "null";
        }
    }

    @Override
    public String getHelp() {
        return "power <number> <exponent>\nReturns number to the power of exponent.  Returns null if non number found.";
    }
    
}
